﻿using System;
namespace UnitClassLibrary
{
    interface IAngle
    {
        double Degrees { get; }
        double Radians { get; }
    }
}
